/// node -i -e "$(cat Dillabot2.js)"
// mysql.server start # for local
var _ = require('minimist')(process.argv);
var _V = _._.includes('verbose');
// var chokidar = require('chokidar');

// var DubAPI = require('./DubAPI/index.js');
var DubAPI = require('dubapi');
var M = require('moment');
var mysql = require('mysql');
var Q = require('q');
var R = require('request');
var Moment = M;

var D = {
    scClientId: '02gUJC0hH2ct1EGOcYXQIzRFU91c72Ea',
    botname: 'Dillabot',
    botlogin: 'bar',
    room: 'beats',
    dbLogin: {host: 'localhost', user: 'root', password: 'bar', database: 'dillabot'},

    // === END OF CREDENTIALS === //

    _getTrig: /(!.+?(\s|$))/uigm,
    _newTrig: /^!(\+|trigger)\s(.+?)\s(.+?)$/ui,
    _echo: /^!(-|echo)\s(.+?)$/ui,
    _delTrig: /^!(\+|trigger)\s(\S+)$/ui,
    _eval: /({\w+?})/g,
    _preEval: /{-(\w+?)-}/g,

    API: null, DB: null,

    __manpage: 'http://www.kaagaz.io/8cdcc43d',
    init(bot) {
        this.API = bot;
        this.DB = new DB(this.dbLogin)
        _V && console.log(`this.API.version : ${this.API.version}`);
        this.bindEvents();

        this.API.connect(this.room);
    },
    bindEvents() {
        this.API.on('error', (err) => console.error('error: ', err));
        this.API.on('connected', (room) => {
            console.log(`@@${M()._d}connected to ${room} as ${this.__manpage}`)
    //        this.API.sendChat('im in, whattyall playin?');
  //        this.API.sendChat('!theme');
//        this.API.sendChat('uhh..');
        });
        this.API.on('disconnected', (room) => console.log(`disconnected from ${room}`));

        this.API.on(this.API.events.userKick, () => this.API.sendChat('https://media0.giphy.com/media/60DO6stFOSFIA/200_s.gif'));
        this.API.on(this.API.events.roomPlaylistUpdate, (e) => {
            if (!e.media) return
            this._link(this.Play.media.type, this.Play.media.fkid).done((url) => {
                this.link = url;
            });
            this._link(e.media.type, e.media.fkid).done((link) => {
                let data = [
                    e.user.id,
                    e.media.id,
                    e.media.name,
                    e.user.username,
                    link,
                    e.score && e.score.grbs || 0
                ]; //dj_id, song_id, song_name, dj_name, song_link, grabs
                this.DB.addPlaysHistory(data)
            })
        });

        this.API.on(this.API.events.chatMessage, (d) => {
            if (!d.user) return
            _V && console.log(d.user.username + ': ' + d.message)
            if (d.user.id == this.API._.self.id) {
                return
            }
            this.triggerMsg = d.message
            this.processMessage(d)
        });
    },
    processMessage(d) {
        // store raw in datamining table
        this.asker = d.user
        var msg = d.message.trim();
        if (match = msg.match(this._echo)) {
            this.processTrig(match[2])
            .then((msg) => this.API.sendChat(msg))
        } else if (match = msg.match(this._newTrig)) {
            _V && console.log('_newTrig')
            newTrig = match.splice(2); // [trigger, echo]
            this.DB.getTrig(newTrig[0])
            .then((r) => {
                if (!r.er && r.res && !r.res[0]) {
                    this.preProcess(newTrig[1]).then((parsedEcho) => {
                        newTrig[1] = parsedEcho;
                        this.DB.newTrig(newTrig, d).then(() => this._msg('trigCreated', newTrig[0]))
                    })
                } else if (!r.er && r.res && r.res[0]) {
                    this._msg('trigExists', r.res[0], d)
                }
            })

        } else if (match = msg.match(this._delTrig)) {
            _V && console.log('_delTrig')
            var delTrig = match.pop();  // [trigger]
            this.DB.delTrig(delTrig)
                .done(() => this._msg('trigDeleted', delTrig, d))

        } else if (match = msg.match(this._getTrig)) {
            _V && console.log('_getTrig')
            allUnique = [...new Set(match.filter((l) => (l + '').trim() != ''))];
            allUnique.forEach((trig) => this.callTrig(trig.substr(1)));
        }
    },
    callTrig(trig) {
        _V && console.log('getting trig => ' + trig)
        // lookup trig
        this.DB.getTrig(trig)
        .then((r) => {
            if(!r.er && r.res && r.res[0]) {
                this.processTrig(r.res[0]['echo'])
                .then((msg) => this.API.sendChat(msg))
            }
        })
    },
    preProcess(txt) {
        _V && console.log('pre-processTrig')
        var d = Q.defer()

        if (evals = txt.match(this._preEval)) {
            evalsUnique = [...new Set(evals)].map((e) => e.replace(/-/g, '')); // emulate
            promises = evalsUnique.map((e) => this[e]).filter((e) => !!e) // evaluate
        } else {
            promises = [];
        }

        function newRegex(rgx) {
            var a = new RegExp(rgx.source.replace(/({(\w+?)})/, '{-$2-}'), rgx.flags)
            return a;
        }

        Q.allSettled(promises).then((arg) => {
            arg.forEach((r) => {
                if (r.state === 'fulfilled') {
                    txt = txt.replace(newRegex(r.value.regex), r.value.v);
                } else {
                    txt = txt.replace(newRegex(r.reason.regex), r.reason.v);
                }
            })
            d.resolve(txt);
        });
        return d.promise;
    },
    processTrig(txt) {
        var d = Q.defer()
        // find evaluateables
        _V && console.log('processTrig')
        if (evals = txt.match(this._eval)) {
            evalsUnique = [...new Set(evals)];
            promises = evalsUnique.map((e) => this[e]).filter((e) => !!e)
        } else {
            promises = [];
        }
        Q.allSettled(promises).then((arg) => {
            arg.forEach((r) => {
                if (r.state === 'fulfilled') {
                    txt = txt.replace(r.value.regex, r.value.v);
                } else {
                    txt = txt.replace(r.reason.regex, r.reason.v);
                }
            })
            d.resolve(txt);
        });
        return d.promise;
    },
    _msg(type, a, b) {
        this.API.sendChat({
            trigExists(a, b) {
                return `"${a.trig}" exists, created by ${a.author} :clock1: ${M(a.created_at).calendar()}`;
            },
            trigDeleted(a, b) {
                return `trigger "${a}" deleted`;
            },
            trigCreated(a, b) {
                return `trigger "${a}" created`;
            },
        }[type](a, b))
    },
    _link(type, fkid) {
        var d = Q.defer();
        if(type == 'youtube') {
            var url = 'youtu.be/' + fkid // var link = 'https://www.youtube.com/watch?v=' + fkid
            d.resolve(url);
        }else if(type == 'soundcloud') {
            var url = `http://api.soundcloud.com/tracks/${fkid}.json?client_id=${this.scClientId}`
            R.get({json: true, uri: url}, function (er, re, songData) {
                d.resolve(songData.permalink_url);
            });
        }
        return d.promise;
    },
    // facedes
    get Play() {
        return this.API._.room.play
    },
    // EVALUATEABLES -- return promise pattern
    get '{link}'() {
        var regex = new RegExp('{link}', 'gm')
        var d = Q.defer();
        if (this.Play) {
            d.resolve({regex, v: this.link});
        } else {
            d.reject({regex, v: '{there is no link}'});
            // take link from history
        }
        return d.promise;
    },
    get '{dj}'() {
        var regex = new RegExp('{dj}', 'gm')
        var d = Q.defer();
        if (this.Play) {
            d.resolve({regex, v: this.API.getDJ().username})
        } else {
            d.reject({regex, v: this.API.getDJ().username})
            var dj = '{there is no dj}';
        }
        return d.promise;
    },
    get '{thumb}'() {
        var regex = new RegExp('{thumb}', 'gm');
        var d = Q.defer();
        if (this.Play) {
            d.resolve({regex, v: this.Play.media.images.thumbnail})
        } else {
            d.reject({regex, v: '{no thumb}'})
        }
        return d.promise;
    },
    get '{djThumb}'() {
        var regex = new RegExp('{djThumb}', 'gm');
        var d = Q.defer();
        if (this.Play) {
            d.resolve({regex, v: this.API.getDJ().profileImage.url})
        } else {
            d.reject({regex, v: '{no dj}'})
        }
        return d.promise;
    },
    get '{myThumb}'() {
        var regex = new RegExp('{myThumb}', 'gm');
        var d = Q.defer();
        d.resolve({regex, v: this.asker.profileImage.url})
        return d.promise;
    },
    get '{lastMsg}'() {
        var regex = new RegExp('{lastMsg}', 'gm');
        var d = Q.defer();
        var v = this.API.getChatHistory()
            .reverse().find((msg) => {
                return (msg.message != this.triggerMsg) &&
                (msg.user.username != this.botname);
            }).message
        d.resolve({regex, v})
        return d.promise;
    },
    get '{manPage}'() {
        var regex = new RegExp('{manPage}', 'gm');
        var d = Q.defer();
        d.resolve({regex, v: this.__manpage})
        return d.promise;
    },
    // update 19th july OLD:'http://www.kaagaz.io/d125496e'
    get '{forget}'() {
        var regex = new RegExp('{plays}', 'gm');
        var d = Q.defer();
        // if bot is above or match a moderator
        this.API.moderateDeleteChat('chat id..');
        d.resolve({regex, v: 'removed n chat'});
        return d.promise;
    },
    get '{plays}'() {
        var regex = new RegExp('{plays}', 'gm');
        var d = Q.defer();
        var M = this.API.getMedia();
        if (M && M.id) {
            this.DB.getPlaysById(M.id).done((res) => {
		var arr = res.res;
		console.log(arr, 'arr');
		var last = arr[0];
		console.log(last, 'last')
		var total = arr.length;
		var newMsg = last.song_name + ' Plays: ' + total;
		if (total > 1) {
			var lastReal = arr[1];
			newMsg += ' Last spin by: *' + lastReal.dj_name + '* :clock: ' + Moment(lastReal.timestamp).calendar();
		
		}
		d.resolve({regex, v: newMsg});
                //this.API.sendChat(''+res.res.length);
            });
        }
        return d.promise;
    },
    get '{updub}'() {
        this.API.updub();
        var regex = new RegExp('{updub}', 'gm');
        var d = Q.defer();
        d.resolve({regex, v: ''})
        return d.promise;
    },
    get '{triggersCount}'() {
        // select count(*) from triggers
    },
    get '{triggersRecent}'() {
        // select count(*) from triggers
    },
    get '{triggersSpreadsheet}'() {
        // echo link
    },
}

function DB(login) {
    this._c = mysql.createConnection(login);
    this._getTrig = 'select * from triggers where trig = ? limit 1';
    this._newTrig = 'insert into triggers (trig, echo, author, author_id) values (?,?,?,?)';
    this._delTrig = 'delete from triggers where trig = ?';
    this._addPlaysHistory = 'insert into playhistory (dj_id, song_id, song_name, dj_name, song_link, grabs) values (?,?,?,?,?,?)';
    this._getPlaysById = "select * from playhistory where song_id=? order by timestamp desc";
}

DB.prototype.getPlaysById = function(id) {
    var d = Q.defer()
    this._c.query(this._getPlaysById, [id], (er, res) => {
        d.resolve({er, res});
    })
    return d.promise;
}

DB.prototype.newTrig = function(newTrig, data) {
    var d = Q.defer()
    this._c.query(this._newTrig, [
        newTrig[0],
        newTrig[1],
        data.user.username,
        data.user.id
    ], (er, res) => {
        d.resolve({er, res});
    })
    return d.promise;
}

DB.prototype.delTrig = function(trig) {
    var d = Q.defer()
    this._c.query(this._delTrig, [trig], (er, res) => {
        d.resolve({er, res});
    })
    return d.promise;
}

DB.prototype.getTrig = function(trig) {
    var d = Q.defer()
    this._c.query(this._getTrig, [trig], (er, res) => {
        d.resolve({er, res});
    })
    return d.promise;
}

DB.prototype.addPlaysHistory = function(data) {
    var d = Q.defer()
    this._c.query(this._addPlaysHistory, data, (er, res) => {
        d.resolve({er, res});
    })
    return d.promise;
}

// var EXPOSED = {
//     sendChat: D.API.sendChat
// }

// chokidar.watch('./cli').on('change', (path) => {
//     fs.readFile(path, 'utf8', function(err, data) {
//         if(data) {
//             if (socket) {
//                 socket.write(data)
//                 logfile(data)
//             }
//         }
//     });
// });

new DubAPI({username: D.botname, password: D.botlogin}, function(err, bot) {
    if (err) return console.error('ERROR: ', err);
    D.init(bot);
});
/* ALL EVENTS

chatMessage: 'chat-message',
chatSkip: 'chat-skip',
deleteChatMessage: 'delete-chat-message',
roomPlaylistDub: 'room_playlist-dub',
roomPlaylistGrab: 'room_playlist-queue-update-grabs',
roomPlaylistQueueUpdate: 'room_playlist-queue-update-dub',
roomPlaylistUpdate: 'room_playlist-update',
roomUpdate: 'room-update',
userBan: 'user-ban',
userImageUpdate: 'user-update',
userJoin: 'user-join',
userKick: 'user-kick',
userLeave: 'user-leave',
userMute: 'user-mute',
userSetRole: 'user-setrole',
userUnban: 'user-unban',
userUnmute: 'user-unmute',
userUnsetRole: 'user-unsetrole',
userUpdate: 'user_update' }


for !plays .... this.API.getPlayID() and lookup dataMine =/



V-0.0.2 Manpage

* Bot listens room user messages
* Bot would echo all matched triggers
* Create new trigger by syntax ✎!trigger triggerName .. and rest will echo, including these two dots
* !+ is an alias of !trigger (In creating/removing trigger, line must start with one of these)
* !- is an alias of !echo (just evaluates and returns)
* An echoed text may contain {expressions} that will evaluate into:

** {dj} - current DJ's name
** {link} - currently played link
** {thumb} - current song thumbnail
** {djThumb} - profile image of current DJ
** {myThumb} - profile image of the one currently triggering bot
** {lastMsg} - most recent (non bot, non current trigger) message in this chat
** {manPage} - this man-page url
*** one might type-out feature-requests in triggers: requests1, requests2 ... , i'll look them up.
* if you want you can evaluate them before new trigger is saved, refer them by using {-expressions-} syntax,
* just use brace+dash instead of simple brace
*
*/
